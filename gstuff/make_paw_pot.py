import numpy as np
from gpaw.atom.generator2 import generate  # , DatasetGenerationError
from ase.data import atomic_numbers, chemical_symbols
from pathlib import Path

#
#

parameters: list[tuple[str, float]] = [
    ('', 0.0),
    ('1s,s,p,D', 0.9),  # H (1. period)
    ('1s,s,p,D', 1.3),  # He
    ('1s,2s,p,D', 1.5),  # Li (2. period)
    ('1s,2s,2p,D', 1.3),  # Be
    ('2s,s,2p,p,d,F', 1.5),  # B
    ('2s,s,2p,p,d,F', 1.2),  # C
    ('2s,s,2p,p,d,F', 1.1),  # N
    ('2s,s,2p,p,d,F', 1.1),  # O
    ('2s,s,2p,p,d,F', 1.2),  # F
    ('2s,s,2p,p,d,F', 1.7),  # Ne
    ('2s,3s,2p,3p,d,F', 2.2),  # Na (3. period)
    ('2s,3s,2p,3p,d,F', 2.2),  # Mg
    ('3s,s,3p,p,d,F', 2.2),  # Al
    ('3s,s,3p,p,d,F', 2.0),  # Si
    ('3s,s,3p,p,d,F', 1.8),  # P
    ('3s,s,3p,p,d,F', 1.7),  # S
    ('3s,s,3p,p,d,F', 1.6),  # Cl
    ('3s,s,3p,p,d,F', 1.5),  # Ar
    ('3s,4s,3p,4p,d,F', 1.8),  # K (4. period)
    ('3s,4s,3p,4p,3d,d,F', 1.9),  # Ca
    ('3s,4s,3p,4p,3d,d,F', 2.3),  # Sc
    ('3s,4s,3p,4p,3d,d,F', 2.2),
    ('3s,4s,3p,4p,3d,d,F', 2.1),
    ('3s,4s,3p,4p,3d,d,F', 2.0),
    ('3s,4s,3p,4p,3d,d,F', 2.0),
    ('3s,4s,3p,4p,3d,d,F', 2.0),
    ('3s,4s,3p,4p,3d,d,F', 2.0),
    ('3s,4s,3p,4p,3d,d,F', 2.0),
    ('3s,4s,3p,4p,3d,d,F', 2.0),  # Cu
    ('3s,4s,3p,4p,3d,d,F', 2.1),  # Zn
    ('4s,s,4p,p,3d,d', 2.0),  # Ga
    ('4s,s,4p,p,3d,d', 1.9),  # Ge
    ('4s,s,4p,p,3d,d', 1.9),  # As
    ('4s,s,4p,p,d,F', 2.0),  # Se
    ('4s,s,4p,p,d,F', 2.1),  # Br
    ('4s,s,4p,p,d,F', 2.2),  # Kr
    ('4s,5s,4p,5p,d,F', 2.3),  # Rb (5. period)
    ('4s,5s,4p,5p,4d,d,F', 2.4),  # Sr
    ('4s,5s,4p,5p,4d,d,F', 2.3),  # Y
    ('4s,5s,4p,5p,4d,d,F', 2.3),  # Zr
    ('4s,5s,4p,5p,4d,d,F', 2.3),  # Nb
    ('4s,5s,4p,5p,4d,d,F', 2.3),  # Mo
    ('4s,5s,4p,5p,4d,d,F', 2.3),  # Tc
    ('4s,5s,4p,5p,4d,d,F', 2.3),  # Ru
    ('4s,5s,4p,5p,4d,d,F', 2.3),  # Rh
    ('4s,5s,4p,5p,4d,d,F', 2.3),  # Pd
    ('4s,5s,4p,5p,4d,d,F', 2.3),  # Ag
    ('4s,5s,4p,5p,4d,d,F', 2.3),  # Cd
    ('4s,5s,4p,5p,4d,d,F', 2.2),  # In
    ('4s,5s,4p,5p,4d,d,F', 2.2),  # Sn
    ('4s,5s,4p,5p,4d,d,F', 2.2),  # Sb
    ('4s,5s,4p,5p,4d,d,F', 2.2),  # Te
    ('5s,s,5p,p,4d,d,F', 2.5),  # I
    ('5s,s,5p,p,4d,d,F', 2.5),  # Xe
    ('5s,6s,5p,6p,5d,d,f', 1.9),  # Cs (6. period)
    ('5s,6s,5p,6p,5d,d,f', 2.0),  # Ba
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),  # La
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),  # Ce
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),  # Pr
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),  # Nd
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),  # Pm
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),  # Sm
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),  # Eu
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),  # Gd
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),  # Tb
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),  # Dy
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),  # Ho
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),  # Er
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),  # Tm
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),  # Yb
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),  # Lu
    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.3),  # Hf
    ('5s,6s,5p,6p,5d,d,F', 2.3),  # Ta
    ('5s,6s,5p,6p,5d,d,F', 2.2),  # W
    ('5s,6s,5p,6p,5d,d,F', 2.2),  # Re
    ('5s,6s,5p,6p,5d,d,F', 2.2),  # Os
    ('5s,6s,5p,6p,5d,d,F', 2.2),  # Ir
    ('5s,6s,5p,6p,5d,d,F', 2.2),  # Pt
    ('5s,6s,5p,6p,5d,d,F', 2.2),  # Au
    ('5s,6s,5p,6p,5d,d,F', 2.2),  # Hg
    ('6s,s,6p,p,5d,d', 2.5),  # Tl
    ('6s,s,6p,p,5d,d', 2.5),  # Pb
    ('6s,s,6p,p,5d,d', 2.5),  # Bi
    ('6s,s,6p,p,d', 2.5),  # Po
    ('6s,s,6p,p,d', 2.4),  # At
    ('6s,s,6p,p,d', 2.3),  # Rn
    ('6s,7s,6p,p,d,f', 2.3),  # Fr
    ('6s,7s,6p,p,d,f', 2.3),  # Ra
    ('6s,7s,6p,p,6d,d,5f,f', 2.3),  # Ac
    ('6s,7s,6p,p,6d,d,5f,f', 2.3),  # Th
    ('6s,7s,6p,p,6d,d,5f,f', 2.3),  # Pa
    ('6s,7s,6p,p,6d,d,5f,f', 2.3),  # U
    ('6s,7s,6p,p,6d,d,5f,f', 2.3),  # Np
    ('6s,7s,6p,p,6d,d,5f,f', 2.3),  # Pu
    ('6s,7s,6p,p,6d,d,5f,f', 2.3),  # Am
    ('6s,7s,6p,p,6d,d,5f,f', 2.3)]  # Cm


def make(symbol, folder, projectors=None, rcut=None):
    Z = atomic_numbers[symbol]
    print(symbol, Z)
    if projectors is None:
        projectors, rcut = parameters[Z]
    f = Path(f'{folder}/{symbol}')
    # if not f.is_dir():
    #     return
    xml = f / f'{symbol}.PBE'
    # if xml.is_file():
    #    return
    r0 = 0.8 * rcut
    for sr in [False, True]:
        gen = generate(symbol,
                       Z,
                       projectors,
                       radii=rcut,
                       r0=r0,
                       v0=None,
                       nderiv0=5 if projectors[-1].isupper() else 2,
                       xc='PBE',
                       scalar_relativistic=sr,
                       pseudize=('poly', 4))
        if not sr:
            try:
                if not gen.check_all(tol=0.11):  # eV
                    print('ERROR:', symbol)
                    continue
            except np.linalg.LinAlgError:
                print('ERROR:', symbol)
                continue

    if 1:
        f.mkdir(exist_ok=True, parents=True)
        setup = gen.make_paw_setup(None)
        setup.generatordata = f'-P {projectors} -r {rcut}'
        setup.write_xml(xml)


def main():
    for symbol in chemical_symbols[1:97]:
        if symbol == 'Ac':
            continue
        make(symbol, 'c1')


def main1():
    for symbol in chemical_symbols[57:72]:
        if symbol == 'Ac':
            continue
        for i, (p, r) in enumerate([('5s,6s,5p,6p,5d,d,4f,f,G', 2.2),
                                    ('5s,6s,5p,6p,5d,d,4f,f', 2.2),
                                    ('5s,6s,5p,6p,5d,d,4f,G', 2.2),
                                    ('5s,6s,5p,6p,5d,d,4f', 2.2),
                                    ('5s,6s,5p,6p,5d,4f,f,G', 2.2),
                                    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.1),
                                    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.3),  # 6
                                    ('5s,6s,5p,6p,5d,d,4f,f,G', 1.9),
                                    ('5s,6s,5p,6p,5d,d,4f,f,G', 2.5),
                                    ('5s,6s,5p,6p,5d,d,4f,f,G', 3.0),
                                    ('5s,6s,5p,6p,5d,d,4f,f,G', 3.5)]):
            if i < 9:
                continue
            make(symbol, f're{i}', p, r)


def main2():
    for symbol in chemical_symbols[49:55]:
        for i, (p, r) in enumerate([('5s,s,5p,p,4d,d', 2.2),
                                    ('5s,s,5p,p,4d,d,F', 2.2),
                                    ('5s,s,5p,p,4d,d,F', 2.3),
                                    ('5s,s,5p,p,4d,d', 2.4),
                                    ('5s,s,5p,p,4d,d', 2.3),
                                    ('5s,s,5p,p,4d,d', 2.1),
                                    ('4s,5s,4p,5p,4d,d,F', 2.2),  # 6
                                    ('5s,s,5p,p,4d,d', 2.0),
                                    ('5s,s,5p,p,4d,d,F', 2.5)]):
            if i < 7:
                continue
            make(symbol, f'inxe{i}', p, r)


def main3():
    for symbol in ['He', 'Be', 'Ne']:
        make(symbol, 'b3')


def main4():
    for symbol in ['Rb', 'Sr']:
        for i, (p, r) in enumerate(
            [('4s,5s,4p,5p,d,F', 2.1),
             ('4s,5s,4p,5p,4d,F', 2.1),
             ('4s,5s,4p,5p,4d,d,F', 2.1),
             ('4s,5s,4p,5p,4d,d,F', 2.4)]):
            make(symbol, f'rbsr{i}', p, r)


def main5():
    for symbol in ['He']:
        for i, (p, r) in enumerate(
            [('1s,s,p,D', 1.1),
             ('1s,s,p', 1.3),
             ('1s,s,p', 1.1)]):
            make(symbol, f'he{i}', p, r)
    for symbol in ['Be']:
        for i, (p, r) in enumerate(
            [('2s,s,2p,D', 1.3),
             ('2s,s,2p,D', 1.1),
             ('2s,s,2p', 1.3),
             ('1s,2s,2p,D', 1.3),
             ('1s,2s,2p,D', 1.1)]):
            make(symbol, f'be{i}', p, r)
    for symbol in ['Be']:
        for i, (p, r) in enumerate(
            [('2s,s,2p,p,d,F', 1.5),
             ('2s,s,2p,p,d,F', 1.3),
             ('2s,s,2p,p,d', 1.3),
             ('2s,s,2p,p,D', 1.3)]):
            make(symbol, f'ne{i}', p, r)


if __name__ == '__main__':
    main()
