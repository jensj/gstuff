from math import pi
from pathlib import Path
from ase.build import bulk
from ase.db import connect
from ase.units import Ha, Bohr
from gpaw import GPAW, PW, Davidson, setup_paths

ecut = 1000
k = 10

# Data from: http://dx.doi.org/10.1016/j.commatsci.2014.07.030
names = ['La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd',
         'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu']
data = dict(zip(names,
                [(10.0457, 10.0465, 122, 122, 0.00, 0.00),
                 (9.5612, 9.5609, 145, 150, 0.27, 0.40),
                 (9.5888, 9.5898, 120, 123, 1.93, 1.91),
                 (9.6424, 9.6432, 123, 122, 3.00, 3.00),
                 (9.5234, 9.5237, 137, 132, 4.00, 4.00),
                 (9.5177, 9.5191, 122, 126, 5.00, 5.00),
                 (9.5435, 9.5451, 117, 119, 6.00, 6.00),
                 (9.4306, 9.4317, 143, 147, 7.00, 7.00),
                 (9.2739, 9.2746, 140, 142, 6.03, 6.05),
                 (9.2383, 9.2401, 143, 145, 5.01, 5.01),
                 (9.2233, 9.2235, 151, 152, 4.00, 4.00),
                 (9.1641, 9.1661, 147, 149, 3.00, 3.00),
                 (9.1124, 9.1123, 143, 146, 1.93, 1.90),
                 (9.0795, 9.0810, 140, 145, 0.22, 0.14),
                 (9.0103, 9.0108, 170, 171, 0.00, 0.00)]))


def run():
    setup_paths[:0] = ['.', '../N']
    db = connect('ren.db')
    symbol, _, setups = Path.cwd().name.partition('.')
    setups = setups or {}
    for s in range(94, 108, 2):
        aref = data[symbol][0] * Bohr
        atoms = bulk(symbol + 'N', 'rocksalt', a=aref)
        i = names.index(symbol)
        M = 7 - abs(i - 7)
        atoms.set_initial_magnetic_moments([M, 0])
        id = db.reserve(name=symbol, s=s)
        if id is None:
            continue
        h = pi / (4 * ecut / Ha)**0.5 * Bohr
        x = (s / 100)**(1 / 3)
        atoms.set_cell(atoms.cell * x, scale_atoms=True)
        atoms.calc = GPAW(mode=PW(ecut),
                          kpts=(k, k, k),
                          setups=setups,
                          h=h * x,
                          xc='PBE',
                          eigensolver=Davidson(4),
                          occupations={'name': 'fermi-dirac', 'width': 0.02},
                          symmetry={'do_not_symmetrize_the_density': True},
                          txt=f'{symbol}N-{s:03}.txt')
        atoms.get_forces()
        atoms.get_stress()
        db.write(atoms=atoms, id=id, name=symbol, s=s)


if __name__ == '__main__':
    run()
