from pathlib import Path

from ase.data import chemical_symbols
from geebaw.eos import eos
from gpaw import GPAW, setup_paths
from myqueue.workflow import run

from gstuff.acwf_structures import acwf_structures, reference_structure


def workflow():
    run(function=run_eos, cores=16, tmax='2h')


def run_eos(symbol: str | None = None) -> dict:
    symbol = symbol or Path.cwd().name
    setup_paths[:0] = ['.']
    results = {}
    for name in acwf_structures:
        atoms = reference_structure(symbol, name)
        params = {
            'kpts': {'density': 8.0},
            'mode': {'name': 'pw', 'ecut': 1000},
            'occupations': {'name': 'fermi-dirac', 'width': 61.2e-3},
            'xc': 'PBE'}
        if symbol in chemical_symbols[58:72]:
            params['convergence'] = {'eigenstates': 1e-6, 'density': 1e-3}
        atoms.calc = GPAW(**params, txt=f'{name}.txt')
        try:
            r = eos(atoms)
        except Exception as ex:
            print('Error', ex)
        else:
            results[name] = r
    return results


if __name__ == '__main__':
    run_eos('H')
