from ase import Atoms
from ase.io import read
from gpaw import GPAW


def conv(params, atoms, tag='conv'):
    if not isinstance(atoms, Atoms):
        atoms = read(atoms)
    params = params.copy()
    data = []
    for ecut in range(300, 1100, 100):
        params['mode']['ecut'] = ecut
        atoms.calc = GPAW(**params, txt=f'{tag}.txt')
        e = atoms.get_potential_energy()
        data.append((ecut, e))
    return data


def main():
    conv({'kpts': {'density': 2.0},
          'mode': {'name': 'pw'}},
         Atoms('H', cell=[1, 1, 1], pbc=True))


if __name__ == '__main__':
    main()
