import numpy as np
from gpaw.atom.generator2 import generate  # , DatasetGenerationError
from ase.data import atomic_numbers
from pathlib import Path


def scan(S, P, D, R):
    for r in R:
        for s in S:
            for p in P:
                for d in D:
                    for f in ['F', '']:
                        yield ','.join([s, p, d, f]).rstrip(','), r


def workflow(s=''):
    for p, r in scan(['6s,s', '5s,6s', '5s,6s,s'],
                     ['6p,p', '5p,6p', '5p,6p,p'],
                     ['5d,d'],
                     [2.5, 2.4, 2.3, 2.2, 2.1]):
        name = p.replace(',', '_') + f'-{r:.1f}'
        make(s, p, r, name)


def make(symbol, projectors, rcut, name):
    Z = atomic_numbers[symbol]
    print(symbol, Z)
    f = Path(f'{symbol}/{name}')
    # if not f.is_dir():
    #     return
    xml = f / f'{symbol}.PBE'
    # if xml.is_file():
    #    return
    r0 = 0.8 * rcut
    for sr in [False, True]:
        gen = generate(symbol,
                       Z,
                       projectors,
                       radii=rcut,
                       r0=r0,
                       v0=None,
                       nderiv0=5 if projectors[-1].isupper() else 2,
                       xc='PBE',
                       scalar_relativistic=sr,
                       pseudize=('poly', 4))
        if not sr:
            try:
                if not gen.check_all(tol=0.11):  # eV
                    print('ERROR:', symbol)
                    continue
            except np.linalg.LinAlgError:
                print('ERROR:', symbol)
                continue

    if 1:
        f.mkdir(exist_ok=True, parents=True)
        setup = gen.make_paw_setup(None)
        setup.generatordata = f'-P {projectors} -r {rcut}'
        setup.write_xml(xml)


if __name__ == '__main__':
    workflow('W')
