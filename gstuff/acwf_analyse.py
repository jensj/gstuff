import json
from pathlib import Path
from typing import Sequence

import numpy as np
from ase.data import chemical_symbols, atomic_numbers
from ase.db import connect
from matplotlib import pyplot as plt

# REFS = Path('/home/jensj/bad-paw/refs/')
REFS = Path('/home/jensj/cloud/')
BASE = set(chemical_symbols[1:57]) | set(chemical_symbols[72:84])


def read(name):
    if name.endswith('json'):
        data = json.loads((REFS / name).read_text())
    else:
        data = json.loads(
            (REFS /
             f'results-oxides-verification-PBE-v1-{name}.json').read_text())
        un = json.loads(
            (REFS /
             f'results-unaries-verification-PBE-v1-{name}.json').read_text())
        data['BM_fit_data'].update(un['BM_fit_data'])
    result = data['BM_fit_data']
    print(len(result))
    return result


def errors(d1, d2, prop=0):
    prop = ['min_volume', 'bulk_modulus_ev_ang3', 'bulk_deriv'][prop]
    D = {}
    E = []
    for k, v1 in d1.items():
        s = k.split('-')[0]
        if s not in BASE:
            continue  # pass
        if s == 'O':
            continue
        if '/' not in k:
            pass  # continue
        x1 = v1[prop]
        if k in d2:
            x2 = d2[k][prop]
            e = 2 * (x1 - x2) / (x1 + x2) * 100
            E.append(e)
            D[k] = e
    if 0:
        for k, v in sorted(D.items(), key=lambda k: abs(k[1])):
            print(k, v)
    return D


def solve(x: Sequence[float], y: np.ndarray, f: float) -> float:
    x2 = x[::-1]
    y2 = abs(y)[::-1]
    for i, e in enumerate(y2):
        if e > f:
            break
    else:
        return x[0]

    ecut1 = x2[i]
    ecut2 = x2[i - 1]
    e1 = y2[i]
    e2 = y2[i - 1]
    return ecut1 + (f - e1) * (ecut2 - ecut1) / (e2 - e1)


def check(path: Path) -> dict[str, float | list[float]]:
    db = connect(path / 'check.db')

    try:
        eegg0 = [(row.h, row.eegg) for row in db.select(test='eggbox')]
        eegg0.sort()
        eegg = [e for h, e in eegg0]
    except (AttributeError, KeyError):
        eegg = []
    if len(eegg) != 3:
        eegg = [np.nan, np.nan, np.nan]

    x = [200, 250, 300, 400, 500, 600, 700, 800, 1500]
    try:
        e1 = np.array([db.get(test='pw1', ecut=ecut).energy for ecut in x])
        e2 = np.array([db.get(test='pw2', ecut=ecut).energy for ecut in x])
    except (AttributeError, KeyError):
        return {'egg': eegg,
                'e400': np.nan,
                'ec': np.nan,
                'dec': np.nan}

    de = e2 / 2 - e1
    e400 = abs(de[3] - de[-1])
    ec = solve(x, e1 - e1[-1], 0.01)
    dec = solve(x, de - de[-1], 0.01)
    return {'egg': eegg,
            'e400': e400,
            'ec': ec,
            'dec': dec}


def read_conv(path, out):
    data = {}
    for p in path.glob('*/'):
        symbol = p.name
        data[symbol] = check(p)
    out.write_text(json.dumps(data))


def hist():
    r0 = read('AE-average')
    g0 = read('gpaw')
    g1 = read('gpaw-std.json')
    o1 = read('o1.json')
    o2 = read('o1-nc.json')
    o3 = read('o1-250.json')
    o4 = read('o1-0.1.json')
    # v1 = read('quantum_espresso-PseudoDojo-0.4-PBE-SR-standard-upf-trim')
    #v1 = read('vasp')
    if 0:
        errors(o1, r0)
        return
    fig, axs = plt.subplots(6, 1)
    for ax, d in zip(axs, [g0, g1, o1, o2, o3, o4]):
        E = errors(d, r0, 0)
        # ax.hist(E, bins=180, density=True)
        ax.hist(E, bins=np.linspace(-3, 3, 101), density=True)
        ax.set_xlim(-3, 3)
        ax.set_ylim(0, 1.5)
    plt.show()


def summary():
    r0 = read('AE-average')
    g0 = read('gpaw')
    o1 = read('o1.json')
    E0 = errors(g0, r0, 0)
    E1 = errors(o1, r0, 0)
    print(len(E0), len(E1))
    for x in sorted(E1, key=lambda x: abs(E1[x]))[-250:]:
        s = x.split("-")[0]
        if s in BASE:
            print(f'{atomic_numbers[s]:2} {x:10} {E1[x]:10.2f}')

    for E in [E0, E1]:
        D = []
        for x, e in E.items():
            D.append((atomic_numbers[x.split('-')[0]], e))
        x, y = np.array(D).T
        plt.plot(x, y, 'x')
    plt.plot([1, 96], [0, 0], '-k')
    plt.show()


def conv(*paths):
    for name in ['ec', 'dec']:
        for p in paths:
            data = json.loads(p.read_text())
            D = sorted((atomic_numbers[k], v) for k, v in data.items())
            print(len(D))
            plt.plot([z for z, d in D], [d[name] for z, d in D],
                     label=f'{name}-{p.name}')
    plt.legend()
    plt.show()
    for name in ['e400']:
        for p in paths:
            data = json.loads(p.read_text())
            D = sorted((atomic_numbers[k], v) for k, v in data.items())
            print(len(D))
            plt.plot([z for z, d in D], [d[name] for z, d in D],
                     label=f'{name}-{p.name}')
    plt.legend()
    plt.show()
    for name in ['egg']:
        for p in paths:
            data = json.loads(p.read_text())
            D = sorted((atomic_numbers[k], v) for k, v in data.items())
            print(len(D))
            plt.plot([z for z, d in D], [d[name][0] for z, d in D],
                     label=f'{name}-{p.name}')
    plt.legend()
    plt.show()


if __name__ == '__main__':
    hist()
    # summary()
    # import sys
    # read_conv(*(Path(arg) for arg in sys.argv[1:]))
    # conv(*(Path(arg) for arg in sys.argv[1:]))
